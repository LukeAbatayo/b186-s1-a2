//1

const addToEnd = (myArr, myStr) => {
  if (typeof myStr === 'string') {
    myArr.push(myStr);

    return myArr;
  } else {
    return 'error - can only add strings to an array';
  }
};

//2

const addToStart = (myArr, myStr) => {
  if (typeof myStr === 'string') {
    myArr.unshift(myStr);

    return myArr;
  } else {
    return 'error - can only add strings to an array';
  }
};

// 3

const elementChecker = (myArr, myStr) => {
  if (myArr.length >= 1) {
    if (myArr.find(myStr)) {
      return true;
    }
  } else {
    return 'error - passed in array is empty';
  }
};
